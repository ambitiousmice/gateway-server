package com.mice.gatewayconfigserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@EnableScheduling
@MapperScan("com.mice.*")
public class GatewayConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayConfigServerApplication.class, args);
    }

}
