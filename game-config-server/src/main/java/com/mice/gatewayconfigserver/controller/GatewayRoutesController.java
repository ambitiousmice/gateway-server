package com.mice.gatewayconfigserver.controller;

import com.mice.gatewayconfigserver.model.dto.gatewayroutes.GatewayRouteDefinition;
import com.mice.gatewayconfigserver.service.gatewayroutes.GatewayRoutesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Description
 * @Author mice
 * @Date 2019/12/3 15:04
 * @Version V1.0
 **/
@RestController
@RequestMapping("gatewayRoutes")
public class GatewayRoutesController {

    @Autowired
    private GatewayRoutesService gatewayRoutesService;

    @RequestMapping("list")
    public  List<GatewayRouteDefinition> getRoutes(){
        return gatewayRoutesService.getRoutes();
    }

    @RequestMapping("lastVersion")
    public  Long getLastVersion(){
        return gatewayRoutesService.getVersion();
    }

    @RequestMapping("updateVersion")
    public  void updateVersion(){
        gatewayRoutesService.updateVersion();
    }

    @RequestMapping("clostRoutes")
    public  void closeRoutes(@RequestParam("routes") String routes){
        gatewayRoutesService.closeRoutes(Arrays.asList(routes.split(",")));
        gatewayRoutesService.updateVersion();
    }


}