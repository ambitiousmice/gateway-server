/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : 192.168.0.188:3306
 Source Schema         : dmg_admin

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 10/03/2020 19:08:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dynamic_route_version
-- ----------------------------
DROP TABLE IF EXISTS `dynamic_route_version`;
CREATE TABLE `dynamic_route_version`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键、自动增长、版本号',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dynamic_route_version
-- ----------------------------
INSERT INTO `dynamic_route_version` VALUES (56, '2020-01-18 19:17:14');

-- ----------------------------
-- Table structure for gateway_routes
-- ----------------------------
DROP TABLE IF EXISTS `gateway_routes`;
CREATE TABLE `gateway_routes`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `route_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由id',
  `route_uri` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转发目标uri',
  `route_order` int(11) NULL DEFAULT NULL COMMENT '路由执行顺序',
  `predicates` json NULL COMMENT '断言字符串集合，字符串结构：[{\r\n                \"name\":\"Path\",\r\n                \"args\":{\r\n                   \"pattern\" : \"/zy/**\"\r\n                }\r\n              }]',
  `filters` json NULL COMMENT '过滤器字符串集合，字符串结构：{\r\n              	\"name\":\"StripPrefix\",\r\n              	 \"args\":{\r\n              	 	\"_genkey_0\":\"1\"\r\n              	 }\r\n              }',
  `is_ebl` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `is_del` tinyint(1) NULL DEFAULT NULL COMMENT '是否删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gateway_routes
-- ----------------------------
INSERT INTO `gateway_routes` VALUES (2, 'lobby-server', 'lb://LOBBY-SERVER', 1, '[{\"args\": {\"pattern\": \"/lobby/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1, 0, '2019-12-03 14:42:50', '2019-12-03 14:43:03');
INSERT INTO `gateway_routes` VALUES (3, 'data-server', 'lb://DATA-SERVER', 1, '[{\"args\": {\"pattern\": \"/data/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1, 0, '2019-12-03 14:42:50', '2019-12-03 14:43:03');
INSERT INTO `gateway_routes` VALUES (4, 'game-config-server', 'lb://GAME-CONFIG-SERVER', 1, '[{\"args\": {\"pattern\": \"/gameconfig/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1, 0, '2019-12-03 14:42:50', '2019-12-03 14:43:03');
INSERT INTO `gateway_routes` VALUES (5, 'game-record-server', 'lb://GAME-RECORD-SERVER', 1, '[{\"args\": {\"pattern\": \"/gamerecord/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1, 0, '2019-12-03 14:42:50', '2019-12-03 14:43:03');
INSERT INTO `gateway_routes` VALUES (12, 'lobby-server-vnnnpay', 'lb://LOBBY-SERVER', 1, '[{\"args\": {\"_genkey_0\": \"POST\"}, \"name\": \"Method\"}, {\"args\": {\"pattern\": \"/vn/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1, 0, '2019-12-03 14:42:50', '2019-12-03 14:43:03');
INSERT INTO `gateway_routes` VALUES (16, 'bcbm-server', 'ws://127.0.0.1:8899', 1, '[{\"args\": {\"pattern\": \"/bcbm/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1, 0, '2019-12-03 14:42:50', '2019-12-03 14:43:03');

SET FOREIGN_KEY_CHECKS = 1;
