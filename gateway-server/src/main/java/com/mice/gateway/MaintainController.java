package com.mice.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author mice
 * @Date 2019/12/26 16:54
 * @Version V1.0
 **/
@RestController
public class MaintainController {
    public static boolean MAINTAIN_STATUS = false;
    public static String MAINTAIN_INFO = "";


    @GetMapping("getMaintainInfo")
    public Map<String,Object> getMaintainInfo(){
        Map<String,Object> map = new HashMap<>();

        if (MAINTAIN_STATUS == false){
            map.put("maintainStatus",false);
        }else {
            map.put("maintainStatus",true);
            map.put("maintainInfo",MAINTAIN_INFO);
        }
        return map;
    }

    @PostMapping("doMaintain")
    public String doMaintain(@RequestParam("maintainInfo")String maintainInfo){
        MAINTAIN_STATUS = true;
        MAINTAIN_INFO = maintainInfo;
        return "success";
    }

    @PostMapping("cancelMaintain")
    public String cancelMaintain(){
        MAINTAIN_STATUS = false;
        return "success";
    }
}